import 'package:flutter/material.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';

import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';

void main() => runApp(SlidingUpPanelExample());

class SlidingUpPanelExample extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        systemNavigationBarColor: Colors.grey[200],
        systemNavigationBarIconBrightness: Brightness.dark,
        systemNavigationBarDividerColor: Colors.black,
      ),
    );

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Simple Map with Flutter',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final double _initFabHeight = 120.0;
  double _fabHeight;
  double _panelHeightOpen = 375.0;
  double _panelHeightClosed = 95.0;

  MapController mapController;
  Position _currentUserPosition;
  LatLng _currentScreenPosition = LatLng(35.7063959, 51.411476);
  

  @override
  void initState() {
    super.initState();
    _fabHeight = _initFabHeight;
    mapController = MapController();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[
          SlidingUpPanel(
            maxHeight: _panelHeightOpen,
            minHeight: _panelHeightClosed,
            parallaxEnabled: true,
            parallaxOffset: .5,
            body: _body(),
            panel: _panel(),
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(18.0),
                topRight: Radius.circular(18.0)),
            onPanelSlide: (double pos) => setState(() {
              _fabHeight = pos * (_panelHeightOpen - _panelHeightClosed) +
                  _initFabHeight;
            }),
          ),
          Positioned(
            right: 20.0,
            bottom: _fabHeight,
            child: FloatingActionButton(
              child: Icon(
                Icons.gps_fixed,
                color: Theme.of(context).primaryColor,
              ),
              onPressed: () async {
                await _getCurrentLocation();
                if (_currentUserPosition != null)
                  setState(() {
                    _currentScreenPosition = LatLng(
                      _currentUserPosition.latitude,
                      _currentUserPosition.longitude,
                    );
                    mapController.move(
                      LatLng(
                        _currentUserPosition.latitude,
                        _currentUserPosition.longitude,
                      ),
                      16,
                    );
                  });
              },
              backgroundColor: Colors.white,
            ),
          ),
          //the SlidingUpPanel Titel
          Positioned(
            top: 42.0,
            child: Container(
              padding: const EdgeInsets.fromLTRB(24.0, 18.0, 24.0, 18.0),
              child: Text(
                "Simple Map with Flutter",
                style: TextStyle(fontWeight: FontWeight.w500),
              ),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(24.0),
                boxShadow: [
                  BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, .25), blurRadius: 16.0)
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  _getCurrentLocation() async {
    await Geolocator().checkGeolocationPermissionStatus();
    final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
        .then((Position position) {
      setState(() {
        _currentUserPosition = position;
      });
    }).catchError((e) {
      print(e);
    });
  }

  Widget _panel() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: 12.0,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 30,
              height: 5,
              decoration: BoxDecoration(
                  color: Colors.grey[300],
                  borderRadius: BorderRadius.all(Radius.circular(12.0))),
            ),
          ],
        ),
        SizedBox(
          height: 18.0,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Explore Pittsburgh",
              style: TextStyle(
                fontWeight: FontWeight.normal,
                fontSize: 24.0,
              ),
            ),
          ],
        ),
        SizedBox(
          height: 36.0,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            _button("Popular", Icons.favorite, Colors.blue),
            _button("Food", Icons.restaurant, Colors.red),
            _button("Events", Icons.event, Colors.amber),
            _button("More", Icons.more_horiz, Colors.green),
          ],
        ),
        SizedBox(
          height: 36.0,
        ),
        Container(
          alignment: Alignment.center,
          child: _currentUserPosition == null
              ? Text('There is no any Position')
              : Text(
                  "YOUR POSITION\n" +
                      "LAT: ${_currentUserPosition.latitude}, LNG: ${_currentUserPosition.longitude}",
                  textAlign: TextAlign.center,
                ),
        ),
        SizedBox(
          height: 36.0,
        ),
        Container(
          alignment: Alignment.center,
          child: _currentUserPosition == null
              ? Text('There is no any Position')
              : Text(
                  "CURRENT SCREEN POSITION\n" +
                      "LAT: ${_currentUserPosition.latitude}, LNG: ${_currentUserPosition.longitude}",
                  textAlign: TextAlign.center,
                ),
        )
      ],
    );
  }

  Widget _button(String label, IconData icon, Color color) {
    return Column(
      children: <Widget>[
        Container(
          padding: const EdgeInsets.all(16.0),
          child: Icon(
            icon,
            color: Colors.white,
          ),
          decoration:
              BoxDecoration(color: color, shape: BoxShape.circle, boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(0, 0, 0, 0.15),
              blurRadius: 8.0,
            )
          ]),
        ),
        SizedBox(
          height: 12.0,
        ),
        Text(label),
      ],
    );
  }

  Widget _body() {
    return FlutterMap(
      mapController: mapController,
      options: MapOptions(
        onPositionChanged: (MapPosition _mapPosition, bool val) {
          print(_mapPosition.center.latitude);
          _currentScreenPosition = _mapPosition.center;
        },
        center: LatLng(40.441589, -80.010948),
        zoom: 13,
        maxZoom: 18,
      ),
      layers: [
        TileLayerOptions(
            urlTemplate: "https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png"),
        MarkerLayerOptions(markers: [
          Marker(
            point: _currentScreenPosition,
            builder: (ctx) => Icon(
              Icons.location_on,
              color: Colors.blue,
              size: 48.0,
            ),
            height: 60,
          ),
        ]),
      ],
    );
  }
}
